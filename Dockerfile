FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

RUN mkdir -p /app/code/wallabag
WORKDIR /app/code/wallabag

# renovate: datasource=github-releases depName=wallabag/wallabag versioning=semver
ARG WALLABAG_VERSION=2.6.10

# This uses the 2.4 branch for pg10 support. this commit is right before the 2FA support
RUN curl -L https://github.com/wallabag/wallabag/archive/${WALLABAG_VERSION}.tar.gz | tar -xz --strip-components 1 -f -

# this token is cloudron-buildbot github account. safe to be "lost"
RUN curl --fail -L https://getcomposer.org/download/2.2.7/composer.phar -o /usr/bin/composer && chmod +x /usr/bin/composer && \
    composer config -g github-oauth.github.com 87c40bbff00ed65193f1e01b1db4f8495f86184b && \
    composer install && \
    composer config -g --unset github-oauth.github.com

RUN mv /app/code/wallabag/app/config/parameters.yml /app/code && ln -s /run/wallabag/parameters.yml /app/code/wallabag/app/config/parameters.yml && \
    mv /app/code/wallabag/app/config/config.yml /app/code && ln -s /run/wallabag/config.yml /app/code/wallabag/app/config/config.yml && \
    mv /app/code/wallabag/app/config/config_prod.yml /app/code && ln -s /run/wallabag/config_prod.yml /app/code/wallabag/app/config/config_prod.yml && \
    rm -rf /app/code/wallabag/data && ln -s /app/data/data /app/code/wallabag/data && \
    rm -rf /app/code/wallabag/var/cache && ln -s /run/wallabag/cache /app/code/wallabag/var/cache && \
    rm -rf /app/code/wallabag/var/logs && ln -s /run/wallabag/logs /app/code/wallabag/var/logs && \
    rm -rf /app/code/wallabag/web/uploads && ln -s /app/data/uploads /app/code/wallabag/web/uploads && \
    rm -rf /app/code/wallabag/web/assets && ln -s /app/data/assets /app/code/wallabag/web/assets && \
    ln -s /app/data/custom.css /app/code/wallabag/web/custom.css

RUN chown -R www-data:www-data /app/code

# configure apache
RUN rm /etc/apache2/sites-enabled/*
RUN sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf
COPY apache/mpm_prefork.conf /etc/apache2/mods-available/mpm_prefork.conf
RUN a2enmod rewrite
RUN a2disconf other-vhosts-access-log
ADD apache/wallabag.conf /etc/apache2/sites-enabled/wallabag.conf
RUN echo "Listen 8000" > /etc/apache2/ports.conf

RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_size 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP memory_limit 256M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_execution_time 200 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/wallabag/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

ADD start.sh /app/code/

CMD [ "/app/code/start.sh" ]
