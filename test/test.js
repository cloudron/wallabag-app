#!/usr/bin/env node

/* global it, xit, describe, before, after, afterEach */

'use strict';

require('chromedriver');

const execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    { Builder, By, until, Key} = require('selenium-webdriver'),
    { Options } = require('selenium-webdriver/chrome');

describe('Application life cycle test', function () {
    this.timeout(0);

    const LOCATION = process.env.LOCATION || 'test';
    const TEST_TIMEOUT = 30000;
    const EXEC_ARGS = { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' };
    const USERNAME = 'wallabag';
    const PASSWORD = 'wallabag';
    const TEST_URL = 'https://cloudron.io';

    let app, browser;

    before(function () {
        const chromeOptions = new Options().windowSize({ width: 1280, height: 1024 });
        if (process.env.CI) chromeOptions.addArguments('no-sandbox', 'disable-dev-shm-usage', 'headless');
        browser = new Builder().forBrowser('chrome').setChromeOptions(chromeOptions).build();
        if (!fs.existsSync('./screenshots')) fs.mkdirSync('./screenshots');
    });

    after(function () {
        browser.quit();
    });

    afterEach(async function () {
        if (!process.env.CI || !app) return;

        const currentUrl = await browser.getCurrentUrl();
        if (!currentUrl.includes(app.domain)) return;
        expect(this.currentTest.title).to.be.a('string');

        const screenshotData = await browser.takeScreenshot();
        fs.writeFileSync(`./screenshots/${new Date().getTime()}-${this.currentTest.title.replaceAll(' ', '_')}.png`, screenshotData, 'base64');
    });

    async function waitForElement(elem) {
        await browser.wait(until.elementLocated(elem), TEST_TIMEOUT);
        await browser.wait(until.elementIsVisible(browser.findElement(elem)), TEST_TIMEOUT);
    }

    async function login() {
        await browser.manage().deleteAllCookies();

        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.id('username'));
        await browser.findElement(By.id('username')).sendKeys(USERNAME);
        await browser.findElement(By.id('password')).sendKeys(PASSWORD);
        await browser.findElement(By.xpath('//form[@name="loginform"]')).submit();
        await waitForElement(By.id('news_menu'));
    }

    async function logout() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.id('news_menu'));
        await browser.findElement(By.id('news_menu')).click();
        await waitForElement(By.xpath('//a[@href="/logout"]'));
        await browser.findElement(By.xpath('//a[@href="/logout"]')).click();
        await waitForElement(By.id('username'));
    }

    async function urlExists() {
        await browser.get(`https://${app.fqdn}/view/1`);
        await waitForElement(By.xpath('//h2[text()="Instantly install Apps"]'));
    }

    function getAppInfo() {
        const inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location.indexOf(LOCATION) === 0; })[0];
        expect(app).to.be.an('object');
    }

    async function addLink() {
        await browser.get(`https://${app.fqdn}`);
        await waitForElement(By.xpath('//a[@href="/new"]'));
        await browser.findElement(By.xpath('//a[@href="/new"]')).click();
        await waitForElement(By.id('entry_url'));
        await browser.findElement(By.id('entry_url')).sendKeys(TEST_URL);
        await browser.findElement(By.id('entry_url')).sendKeys(Key.ENTER);
        await waitForElement(By.xpath('//a[@href="/view/1"]'));
    }

    async function registrationDisabled() {
        const response = await superagent.get('https://' + app.fqdn + '/register').redirects(0).ok(() => true);
        if (response.statusCode !== 301) throw new Error('register is not redirecting ' + response.statusCode);
    }

    xit('build app', function () { execSync('cloudron build', EXEC_ARGS); });
    it('install app', function () { execSync('cloudron install --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('backup app', function () { execSync('cloudron backup create --app ' + app.id, EXEC_ARGS); });
    it('restore app', function () {
        const backups = JSON.parse(execSync(`cloudron backup list --raw --app ${app.id}`));
        execSync(`cloudron uninstall --app ${app.id}`, EXEC_ARGS);
        execSync(`cloudron install --location ${LOCATION}`, EXEC_ARGS);
        getAppInfo();
        execSync(`cloudron restore --backup ${backups[0].id} --app ${app.id}`, EXEC_ARGS);
    });

    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('move to different location', function (done) {
        // ensure we don't hit NXDOMAIN in the mean time
        browser.get('about:blank').then(function () {
            execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, EXEC_ARGS);
            done();
        });
    });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('url exists', urlExists);
    it('can logout', logout);
    it('disabled registration', registrationDisabled);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });

    // test update
    it('can install app', function () { execSync('cloudron install --appstore-id org.wallabag.cloudronapp2 --location ' + LOCATION, EXEC_ARGS); });

    it('can get app information', getAppInfo);
    it('can login', login);
    it('can add link', addLink);
    it('url exists', urlExists);
    it('can logout', logout);
    it('can update', function () { execSync('cloudron update --app ' + LOCATION, EXEC_ARGS); });
    it('can login', login);
    it('url exists', urlExists);
    it('disabled registration', registrationDisabled);

    it('uninstall app', async function () {
        // ensure we don't hit NXDOMAIN in the mean time
        await browser.get('about:blank');
        execSync('cloudron uninstall --app ' + app.id, EXEC_ARGS);
    });
});
